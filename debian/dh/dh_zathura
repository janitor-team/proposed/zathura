#!/usr/bin/perl -w
# Copyright 2012-2018 Sebastian Ramacher <sramacher@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# PROMISE: DH NOOP WITHOUT tmp(usr/lib)

=head1 NAME

dh_zathura - helps with packaging zathura plugins

=head1 SYNOPSIS

dh_libva [S<I<debhelper options>>] [B<--substvar=>I<substvar>]

=head1 DESCRIPTION

B<dh_zathura> is a debhelper program that generates dependencies for zathura
plugin packages. It scans packages looking for zathura plugins and adds the
packages providing the necessary ABI for the plugins to the B<${misc:Depends}>
substitution variable.

=head1 OPTIONS

=over 4

=item B<--substvar=>I<substvar>

Override the substitution variable.

=back

=cut

use strict;
use warnings;
use Debian::Debhelper::Dh_Lib;
use Set::Scalar;

init(options => {
    "substvar=s" => \$dh{SUBSTVAR}
});

unless ($dh{SUBSTVAR}) {
  $dh{SUBSTVAR} = "misc:Depends";
}

my $triplet = dpkg_architecture_value("DEB_HOST_MULTIARCH");

foreach my $package (@{$dh{DOPACKAGES}}) {
  # Directory that contains the files included in the package.
  my $pkgpath = tmpdir($package);
  $pkgpath .= "/usr/lib/$triplet/zathura";
  next unless -d $pkgpath;

  # ABI versions
  my $abiver = Set::Scalar->new;

  # Files to check
  my @filelist = glob($pkgpath . "/*.so");
  foreach my $plugin (@filelist) {
    open(my $nm, "-|", "nm", ("-D", $plugin)) or
      error("cannot read symbols: $!\n");
    while (<$nm>) {
      chomp;

      my @data = split(' ');
      next unless scalar(@data) == 3;

      my $symbol = $data[2];
      if ($symbol =~ /^zathura_plugin_(\d+)_(\d+)$/) {
        $abiver->insert("zathura-abi-$2");
      }
    }
    close $nm or error($! ? "cannot close nm pipe: $!\n" :
      "non-zero exit status from nm: $=\n");
  }

  # Add substvar
  addsubstvar($package, $dh{SUBSTVAR},
    join(' | ', reverse sort $abiver->elements));
}

exit 0;

=head1 SEE ALSO

L<debhelper(7)>, L<dh(1)>.

This program is meant to be used together with debhelper.

=head1 AUTHOR

Sebastian Ramacher <sramacher@debian.org>

=cut

# vim:ts=2 sw=2 et
